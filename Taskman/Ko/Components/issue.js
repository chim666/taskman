﻿function issueVm(options) {
    var component = this;

    component.Id = options.Id;
    component.ParentId = options.ParentId ;
    component.Name = ko.observable();
    component.Executed = ko.observable();
    component.IssueStateId = ko.observable();
    component.DateCreate = ko.observable();
    component.Description = ko.observable();

    component.isViewMode = ko.observable(true);
    component.isNew = ko.computed(function () {
        return component.Id() === undefined;
    });
    component.issueStates = options.issueStates;
    component.issues = ko.observableArray([]);


    component.editItem = function() {
        component.isViewMode(false);
    };

    component.deleteSubItem = function (id) {
        $.each(component.issues(), function (index, item) {
            if (item !== undefined && item.Id() === id) {
                if (id === undefined) {
                    component.issues.remove(item);
                } else {
                    issuesImpl.remove(id).done(function () {
                        component.issues.remove(item);
                    }).fail(function (error) { alert(error); });
                }
            }
        });
    };

    component.addItem = function () {
        component.issues.unshift({ Id: ko.observable(), ParentId: component.Id(), Name: '', issueStates: component.issueStates, deleteItem: component.deleteSubItem });
    };

    component.deleteItem = function () {
        options.deleteItem(component.Id());
    };

    component.saveItem = function () {
        if (component.isNew()) {
            issuesImpl.insert(component.toJS()).done(function (data) {
                component.Id(data.Id);
                component.DateCreate(data.DateCreate);
                component.isViewMode(true);
            }).fail(function(error) { alert(error); });
        } else {
            issuesImpl.update(component.toJS()).done(function () {
                component.isViewMode(true);
            }).fail(function (error) { alert(error); });
        }
    };

    component.cancelEdit = function () {
        if (component.isNew()) {
            component.deleteItem();
        } else {
            component.fromJS(options);
            component.isViewMode(true);
        }
    };

    component.canAdd = ko.computed(function () {
        var result = true;
        $.each(component.issues(), function (index, item) {
            if (item !== undefined && item.Id() === undefined)
                result = false;
        });
        return result;
    });

    component.fromJS = function(data) {
        component.Name(data.Name);
        component.Executed(data.Executed);
        component.DateCreate(data.DateCreate);
        component.Description(data.Description);
        component.IssueStateId(data.IssueStateId);
        if (component.isNew()) {
            component.isViewMode(false);
        }
    };

    component.toJS = function () {
        var data = {
            Id: component.Id(),
            ParentId: component.ParentId,
            Name: component.Name(),
            Executed: component.Executed(),
            DateCreate: component.DateCreate(),
            Description: component.Description(),
            IssueStateId: component.IssueStateId()
        }
        return data;
    };

    component.init = function () {
        component.fromJS(options);
        if (!component.isNew()) {
            var loadOptions = {
                parentId: component.Id()
            };

            issuesImpl.load(loadOptions).done(function(data) {
                component.issues.removeAll();
                $.each(data.IssueModels, function (index, item) {
                    component.issues.push($.extend(item, { Id: ko.observable(item.Id), issueStates: component.issueStates, deleteItem: component.deleteSubItem }));
                });
            }).fail(function (error) { alert(error); });
        }
    }

    component.init();

    executedSubscription = component.Executed.subscribe(function (executed) {
        if (!component.isNew()) {
            component.saveItem();
        }
    });

    component.dispose = function () {
        if (component.executedSubscription != undefined)
            component.executedSubscription.dispose();
    }; 
}

if (!ko.components.isRegistered("issue")) {
    ko.components.register("issue", {
        viewModel: issueVm,
        template: { element: "issue" }
    });
}


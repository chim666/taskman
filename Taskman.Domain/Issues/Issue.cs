﻿using System;

namespace Taskman.Domain.Issues
{
    public sealed class Issue
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public int IssueStateId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreate { get; set; }
        public string Description { get; set; }
        public bool Executed { get; set; }
        
    }
}
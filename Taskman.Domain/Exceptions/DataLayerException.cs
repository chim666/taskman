﻿using System;

namespace Taskman.Domain.Exceptions
{
    public class DataLayerException: Exception
    {
        public DataLayerException(string messgae, Exception innerException) : base(messgae, innerException)
        {
        }
        public DataLayerException(string messgae) : base(messgae)
        {
        }


    }
}
﻿using System.Threading.Tasks;
using Taskman.Domain;

namespace Taskman.Core
{
    public interface IUserRepocitory
    {
        Task<User> SignIn(string username, string password);
    }
}
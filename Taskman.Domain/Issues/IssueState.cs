namespace Taskman.Domain.Issues
{
    public sealed class IssueState
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
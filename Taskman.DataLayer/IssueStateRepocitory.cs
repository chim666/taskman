using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Taskman.Core;
using Taskman.Domain.Issues;

namespace Taskman.DataLayer
{
    public class IssueStateRepocitory : Repocitory, IIssueStateRepocitory
    {
        public IssueStateRepocitory(Func<TaskmanContext> dbFactory) : base(dbFactory)
        {

        }

        public Task<List<IssueState>> GetAll()
        {
            return Query(db =>
            {
                return db.IssueStates.ToListAsync();
            });
        }

        public Task<int> Insert(IssueState issueState)
        {
            return Query(async db =>
            {
                db.IssueStates.Add(issueState);
                await db.SaveChangesAsync();
                return issueState.Id;
            });
        }

        public Task Update(IssueState issueState)
        {
            return Query(db =>
            {
                db.IssueStates.Attach(issueState);
                var entry = db.Entry(issueState);
                entry.State = EntityState.Modified;
                return db.SaveChangesAsync();
            });
        }

        public Task Delete(int id)
        {
            return Query(async db =>
            {
                var issueState = await db.IssueStates.SingleAsync(it=>it.Id == id);
                db.IssueStates.Remove(issueState);
                return db.SaveChanges();
            });
        }
    }
}
﻿function issueStateVm(options) {
    var self = this;

    self.Id = options.Id;
    self.Name = ko.observable();

    self.isViewMode = ko.observable(true);
    self.isNew = ko.computed(function () {
        return self.Id() === undefined;
    });
    
    self.editItem = function() {
        self.isViewMode(false);
    };

    self.deleteItem = function () {
        options.deleteItem(self.Id());
    };

    self.saveItem = function () {
        if (self.isNew()) {
            issueStatesImpl.insert(self.toJS()).done(function (data) {
                self.Id(data);
                self.isViewMode(true);
            });
        } else {
            issueStatesImpl.update(self.toJS()).done(function() {
                self.isViewMode(true);
            });
        }
    };

    self.cancelEdit = function () {
        if (self.isNew()) {
            self.deleteItem();
        } else {
            self.fromJS(options);
            self.isViewMode(true);
        }

    };

    self.fromJS = function(data) {
        self.Name(data.Name);
        if (self.isNew()) {
            self.isViewMode(false);
        }
    };

    self.toJS = function () {
        var data = {
            Id: self.Id(),
            Name: self.Name()
        }
        return data;
    };

    self.fromJS(options);
}

if (!ko.components.isRegistered("issue_state")) {
    ko.components.register("issue_state", {
        viewModel: issueStateVm,
        template: { element: "issue_state" }
    });
}


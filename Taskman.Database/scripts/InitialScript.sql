﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Username] VARCHAR(50) NOT NULL, 
    [Password] VARCHAR(50) NOT NULL
)
go

CREATE TABLE [dbo].[IssueStates]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(50) NOT NULL,
	
)
go

CREATE TABLE [dbo].[Issues]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ParentId] INT NULL, 
    [IssueStateId] INT NOT NULL, 
	[UserId] INT NOT NULL,
    [Name] VARCHAR(100) NULL, 
    [DateCreate] DATETIME NOT NULL, 
    [Description] VARCHAR(MAX) NULL, 
    [Executed] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Issues_ToIssueStates] FOREIGN KEY (IssueStateId) REFERENCES IssueStates(Id), 
    CONSTRAINT [FK_Issues_ToIssue] FOREIGN KEY (ParentId) REFERENCES dbo.Issues(Id)
)
go

insert into dbo.Users
select 1, 'test', 'test' union all
select 2, 'test2', 'test'
go


insert into dbo.IssueStates
select 'Low' union all
select 'Hard'
go
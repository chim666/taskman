﻿using System.Data.Entity;
using Taskman.Domain;
using Taskman.Domain.Issues;

namespace Taskman.DataLayer
{
    public class TaskmanContext: DbContext
    {
        public TaskmanContext()
            :base("name=Taskman")
        {
            Database.SetInitializer<TaskmanContext>(null);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<IssueState> IssueStates { get; set; }
    }
}
﻿using System;

namespace Taskman.Web.Models
{
    public class IssueStateModel
    {
        public int Id { get; set; } 

        public string Name { get; set; }
    }
}
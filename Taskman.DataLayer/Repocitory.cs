﻿using System;
using System.Threading.Tasks;
using Taskman.Domain.Exceptions;

namespace Taskman.DataLayer
{
    public class Repocitory
    {
        private readonly Func<TaskmanContext> _dbFactory;

        public Repocitory(Func<TaskmanContext> dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public async Task<T> Query<T>(Func<TaskmanContext, Task<T>> query)
        {
            using (var db = _dbFactory())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var result = await query.Invoke(db);
                        transaction.Commit();
                        return result;
                    }
                    catch (DataLayerException)
                    {
                        throw;
                    }
                    catch (Exception exception)
                    {
                        transaction.Rollback();
                        throw new DataLayerException("Error in execution query", exception);
                    }
                }
            }
        }
    }
}
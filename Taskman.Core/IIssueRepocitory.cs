﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Taskman.Domain;
using Taskman.Domain.Issues;

namespace Taskman.Core
{
    public interface IIssueRepocitory
    {
        Task<List<Issue>> GetAll(IssueFilter filter, Paging paging);
        Task<int> GetCount(IssueFilter filter);
        Task<Issue> Insert(Issue issue);
        Task Update(Issue issue);
        Task Delete(int id);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Security;
using Taskman.Core;
using Taskman.Domain;
using Taskman.Domain.Issues;
using Taskman.Web.Models;

namespace Taskman.Web.WebApi
{
    [Authorize]
    public class IssueController : ApiController
    {
        private readonly IIssueRepocitory _repocitory;

        private int UserId
        {
            get
            {
                var userId = ((FormsIdentity)(User.Identity)).Ticket.UserData;
                return int.Parse(userId);
            }
        }

        public IssueController(IIssueRepocitory repocitory)
        {
            _repocitory = repocitory;
        }

        // GET: api/IssueState
        public async Task<IssuesModel> Get()
        {
            var filter = new IssueFilter();
            filter.UserId = UserId;
            Dictionary<string, string> options = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value); //parsed options

            if (options.ContainsKey("parentId"))
                filter.ParentId = int.Parse(options["parentId"]);
            if (options.ContainsKey("IssueStateId") && options["IssueStateId"]!= "undefined")
                filter.IssueStateId = int.Parse(options["IssueStateId"]);
            if (options.ContainsKey("DateCreate"))
                filter.DateCreate = DateTime.Parse(options["DateCreate"]);
            if (options.ContainsKey("Name"))
                filter.Name = options["Name"];
            if (options.ContainsKey("Description"))
                filter.Description = options["Description"];
            if (options.ContainsKey("Executed"))
                filter.Executed = bool.Parse(options["Executed"]);

            Paging paging = null;
            if (options.ContainsKey("Skip"))
            {
                paging = new Paging();
                paging.Skip = int.Parse(options["Skip"]);
                paging.Take = int.Parse(options["Take"]);
            }

            return new IssuesModel()
            {
                IssueModels = (await _repocitory.GetAll(filter, paging)).Select(it => new IssueModel()
                {
                    Id = it.Id,
                    Name = it.Name,
                    Description = it.Description,
                    ParentId = it.ParentId,
                    DateCreate = it.DateCreate,
                    Executed = it.Executed,
                    IssueStateId = it.IssueStateId,
                }),
                Count = (await _repocitory.GetCount(filter))
            };

        }

        //// GET: api/IssueState/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/IssueState
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]IssueModel value)
        {
            var result = await _repocitory.Insert(new Issue()
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                ParentId = value.ParentId,
                Executed = value.Executed,
                IssueStateId = value.IssueStateId,
                UserId = UserId,
            });
            return Ok(result);
        }

        // PUT: api/IssueState/5
        public Task Put([FromBody]IssueModel value)
        {
            return _repocitory.Update(new Issue()
            {
                Id = value.Id,
                ParentId = value.ParentId,
                Name = value.Name,
                Description = value.Description,
                Executed = value.Executed,
                IssueStateId = value.IssueStateId,
                UserId = UserId,
            });
        }

        // DELETE: api/IssueState/5
        public Task Delete(int id)
        {
            return _repocitory.Delete(id);
        }
    }
}

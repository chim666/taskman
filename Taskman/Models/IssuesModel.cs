﻿using System;
using System.Collections.Generic;

namespace Taskman.Web.Models
{
    public class IssuesModel
    {
        public IEnumerable<IssueModel> IssueModels { get; set; }
        public int Count { get; set; }
    }
}
﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Taskman.Core;
using Taskman.Domain;

namespace Taskman.DataLayer
{
    public class UserRepocitory: Repocitory, IUserRepocitory
    {
        public UserRepocitory(Func<TaskmanContext> dbFactory) :base(dbFactory)
        {
            
        }

        public Task<User> SignIn(string username, string password)
        {
            return Query( db =>
            {
                return db.Users.SingleOrDefaultAsync(it => it.Username.ToUpper() == username.ToUpper() && it.Password == password);
            });
        }
    }
}
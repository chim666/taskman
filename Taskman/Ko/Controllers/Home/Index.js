﻿var ViewModel = function () {
    var self = this;

    self.issues = ko.observableArray([]);
    self.issueStates = ko.observableArray([]);
    self.issueStatesForFilter = ko.observableArray([]);
    self.executedList = [
        { Value: undefined, Name: 'All' },
        { Value: true, Name: 'Executed' },
        { Value: false, Name: 'Not executed' }];

    self.filter = {
        IssueStateId: ko.observable(),
        DateCreate: ko.observable(undefined),
        Name: ko.observable(),
        Description: ko.observable(),
        Executed: ko.observable(),
    }

    self.paging = new function () {
        var pagingSelf = this;
        pagingSelf.count = ko.observable(0);
        pagingSelf.pageSize = ko.observable(2);
        pagingSelf.pageSizes = ko.observableArray([2, 5, 10]);

        pagingSelf.currentPage = ko.observable(0);
        pagingSelf.pagesCount = ko.computed(function () {
            var result = pagingSelf.count() / pagingSelf.pageSize();
            if (pagingSelf.currentPage() >= result) {
                var back = pagingSelf.currentPage();
                pagingSelf.currentPage(0);
                if (back != pagingSelf.currentPage()) {
                    self.loadIssues();
                }
            }
            return result;
        });
        pagingSelf.pages = ko.computed(function () {
            var array = [];
            for (var i = 0; i < pagingSelf.pagesCount(); i++) {
                array.push({
                    number: i,
                    isSelected: i == pagingSelf.currentPage(),
                    link: i + 1,
                    href: function() {
                        pagingSelf.currentPage(this.number);
                        self.loadIssues();
                    }
                });
            }
            return array;
        });

        pagingSelf.pagingOptions = {
            Skip: function () {return pagingSelf.currentPage() * pagingSelf.pageSize();},
            Take: function () { return pagingSelf.pageSize();}
        }
    }
    
    self.deleteItem = function(id) {
        $.each(self.issues(), function(index, item) {
            if (item !== undefined && item.Id() === id) {
                if (id === undefined) {
                    self.issues.remove(item);
                    self.paging.count(self.paging.count() - 1);
                } else {
                    issuesImpl.remove(id).done(function() {
                        self.issues.remove(item);
                        self.paging.count(self.paging.count() - 1);
                    }).fail(function (error) { alert(error); });
                }
            }
        });
    };

    self.addItem = function() {
        self.issues.unshift({ Id: ko.observable(), Name: '', issueStates: self.issueStates, deleteItem: self.deleteItem });
        self.paging.count(self.paging.count() + 1);
    };

    self.canAdd = ko.computed(function () {
        if (self.issueStates().length == 0)
            return false;
        var result = true;
        $.each(self.issues(), function(index, item) {
            if (item !== undefined && item.Id() === undefined)
                result = false;
        });
        return result;
    });

    self.init = function () {
        issueStatesImpl.load().done(function(data) {
            self.issueStates.removeAll();
            self.issueStatesForFilter.removeAll();
            self.issueStatesForFilter.push({ Id: undefined, Name: 'All' });
            $.each(data, function (index, item) {
                self.issueStates.push(item);
                self.issueStatesForFilter.push(item);
            });
        });

        self.loadIssues();
    }

    self.loadIssues = function () {
        var loadOptions = {
            IssueStateId: self.filter.IssueStateId(),
            DateCreate: self.filter.DateCreate(),
            Name: self.filter.Name(),
            Description: self.filter.Description(),
            Executed: self.filter.Executed(),
        };

        if (loadOptions.DateCreate === null) loadOptions.DateCreate = undefined;
        if (loadOptions.DateCreate != undefined) {
            loadOptions.DateCreate = loadOptions.DateCreate.getFullYear() + '-' + (loadOptions.DateCreate.getMonth() + 1) + '-' + loadOptions.DateCreate.getDate();
        }

        loadOptions = $.extend(loadOptions, self.paging.pagingOptions);
        issuesImpl.load(loadOptions).done(function (data) {
            self.paging.count(data.Count);
            self.issues.removeAll();
            $.each(data.IssueModels, function (index, item) {
                self.issues.push($.extend(item, { Id: ko.observable(item.Id), issueStates: self.issueStates, deleteItem: self.deleteItem }));
            });
        }).fail(function (error) { alert(error); });
    }

    self.init();

    self.pageSizeSubscription = self.paging.pageSize.subscribe(function (executed) {
        self.loadIssues();
    });

    self.dispose = function () {
        if (self.pageSizeSubscription != undefined)
            self.pageSizeSubscription.dispose();
    };
}


$(document).ready(function () {
    var model = new ViewModel();
    ko.applyBindings(model);
});
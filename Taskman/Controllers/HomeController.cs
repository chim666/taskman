﻿using System.Web.Mvc;

namespace Taskman.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        // GET: Issue
        public ActionResult Index()
        {
            return View();
        }
    }
}
﻿CREATE TABLE [dbo].[Issues]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ParentId] INT NULL, 
    [IssueStateId] INT NOT NULL, 
	[UserId] INT NOT NULL,
    [Name] VARCHAR(100) NULL, 
    [DateCreate] DATETIME NOT NULL, 
    [Description] VARCHAR(MAX) NULL, 
    [Executed] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Issues_ToIssueStates] FOREIGN KEY (IssueStateId) REFERENCES IssueStates(Id), 
    CONSTRAINT [FK_Issues_ToIssue] FOREIGN KEY (ParentId) REFERENCES dbo.Issues(Id)
)

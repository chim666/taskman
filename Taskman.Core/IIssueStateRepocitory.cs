﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using Taskman.Domain.Issues;

namespace Taskman.Core
{
    public interface IIssueStateRepocitory
    {
        Task<List<IssueState>> GetAll();
        Task<int> Insert(IssueState issueState);
        Task Update(IssueState issueState);
        Task Delete(int id);
    }
}
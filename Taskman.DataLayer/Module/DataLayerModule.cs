﻿using System;
using Autofac;
using Autofac.Builder;
using Taskman.Core;

namespace Taskman.DataLayer.Module
{
    public class DataLayerModule: Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepocitory>().As<IUserRepocitory>();
            builder.RegisterType<IssueRepocitory>().As<IIssueRepocitory>();
            builder.RegisterType<IssueStateRepocitory>().As<IIssueStateRepocitory>();
            builder.RegisterType<TaskmanContext>();
            builder.RegisterGeneratedFactory<Func<TaskmanContext>>();
        }
    }
}
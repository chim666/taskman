﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Taskman.Core;
using Taskman.Domain.Issues;
using Taskman.Web.Models;

namespace Taskman.Web.WebApi
{
    public class IssueStateController : ApiController
    {
        private readonly IIssueStateRepocitory _repocitory;

        public IssueStateController(IIssueStateRepocitory repocitory)
        {
            _repocitory = repocitory;
        }

        // GET: api/IssueState
        public async Task<IEnumerable<IssueStateModel>> Get()
        {

            return (await _repocitory.GetAll()).Select(it=>new IssueStateModel()
            {
                Id = it.Id,
                Name = it.Name,
            });
        }

        //// GET: api/IssueState/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/IssueState
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]IssueStateModel value)
        {
            var result = await _repocitory.Insert(new IssueState()
            {
                Id = value.Id,
                Name = value.Name,
            });
            return Ok(result);
        }

        // PUT: api/IssueState/5
        public Task Put([FromBody]IssueStateModel value)
        {
            return _repocitory.Update(new IssueState()
            {
                Id = value.Id,
                Name = value.Name,
            });
        }

        // DELETE: api/IssueState/5
        public Task Delete(int id)
        {
            return _repocitory.Delete(id);
        }
    }
}

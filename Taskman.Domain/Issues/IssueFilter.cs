﻿using System;

namespace Taskman.Domain.Issues
{
    public class IssueFilter
    {
        public int? ParentId { get; set; }
        public int? IssueStateId { get; set; }
        public int UserId { get; set; }
        public DateTime? DateCreate { get; set; } 
        public string Name { get; set; } 
        public string Description { get; set; }
        public bool? Executed { get; set; }
        
    }
}
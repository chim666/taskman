﻿var ViewModel = function() {
    var self = this;

    self.issueStates = ko.observableArray([]);

    self.deleteItem = function(id) {
        $.each(self.issueStates(), function(index, item) {
            if (item !== undefined && item.Id() === id) {
                if (id === undefined) {
                    self.issueStates.remove(item);
                } else {
                    issueStatesImpl.remove(id).done(function() {
                        self.issueStates.remove(item);
                    }).fail(function(error) { alert(error) });
                }
            }
        });
    };

    self.addItem = function() {
        self.issueStates.push({ Id: ko.observable(), Name: '', deleteItem: self.deleteItem });
    };

    self.canAdd = ko.computed(function () {
        var result = true;
        $.each(self.issueStates(), function(index, item) {
            if (item !== undefined && item.Id() === undefined)
                result = false;
        });
        return result;
    });

    self.init = function() {
        issueStatesImpl.load().done(function (data) {
            self.issueStates.removeAll();
            $.each(data, function (index, item) {
                self.issueStates.push($.extend(item, { Id: ko.observable(item.Id), deleteItem: self.deleteItem }));
            });
        });
    }

    self.init();
}

$(document).ready(function () {
    var model = new ViewModel();
    ko.applyBindings(model);
});
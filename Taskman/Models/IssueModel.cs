﻿using System;

namespace Taskman.Web.Models
{
    public class IssueModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public int IssueStateId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreate { get; set; }
        public string Description { get; set; }
        public bool Executed { get; set; }
    }
}
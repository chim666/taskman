﻿var issuesImpl = {
    url: getBaseUrl() + 'api/Issue',
    _sendRequest: function (type, params) {
        var deferred = new $.Deferred();
        var requestSettings = {
            url: $.trim(issuesImpl.url),
            type: type,
            dataType: 'json',
            success: function (data) {
                deferred.resolve(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                deferred.reject(jqXHR.responseText);
            }
        };
       
        if (params) {
            var set = false;
            if (type == 'DELETE') {
                if (requestSettings.url[requestSettings.url.length - 1] != '/')
                    requestSettings.url += '/';
                requestSettings.url += params.toString();
                set = true;
            }
            
            if (set === false)
                requestSettings.data = params;
        }
        $.ajax(requestSettings);
        return deferred;
    },
    load: function (loadOptions) {
        return issuesImpl._sendRequest('GET', loadOptions);
    },
    insert: function (values) {
        return issuesImpl._sendRequest('POST', values);
    },
    update: function (values) {
        return issuesImpl._sendRequest('PUT', values);
    },
    remove: function (key) {
        return issuesImpl._sendRequest('DELETE', key);
    },
   
}
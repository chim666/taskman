using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Taskman.Core;
using Taskman.Domain;
using Taskman.Domain.Exceptions;
using Taskman.Domain.Issues;

namespace Taskman.DataLayer
{
    public class IssueRepocitory : Repocitory, IIssueRepocitory
    {
        public IssueRepocitory(Func<TaskmanContext> dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<Issue> Get(TaskmanContext db, IssueFilter filter, Paging paging)
        {
                IQueryable<Issue> querable =  db.Issues;

                // Filter
                querable = querable.Where(it =>
                    (it.ParentId == filter.ParentId || it.ParentId == null && filter.ParentId == null) 
                    && it.UserId == filter.UserId);
                if (filter.IssueStateId.HasValue)
                    querable = querable.Where(it => it.IssueStateId == filter.IssueStateId);
                if (filter.Executed.HasValue)
                    querable = querable.Where(it => it.Executed == filter.Executed);
                if (filter.DateCreate.HasValue)
                    querable = querable.Where(it => it.DateCreate== filter.DateCreate);
                if (!string.IsNullOrEmpty(filter.Name))
                    querable = querable.Where(it => it.Name.Contains(filter.Name));
                if (!string.IsNullOrEmpty(filter.Description))
                    querable = querable.Where(it => it.Description.Contains(filter.Description));


                // Paging
                if (!filter.ParentId.HasValue && paging!= null)
                    querable = querable.OrderBy(it=>it.Id).Skip(paging.Skip).Take(paging.Take);
                
                return querable;
           
        }

        public Task<List<Issue>> GetAll(IssueFilter filter, Paging paging)
        {
            return Query(db => Get(db, filter, paging).ToListAsync());
        }

        public Task<int> GetCount(IssueFilter filter)
        {
            return Query(db => Get(db, filter, null).CountAsync());
        }


        public Task<Issue> Insert(Issue issue)
        {
            issue.DateCreate = DateTime.UtcNow.Date;
            return Query(async db =>
            {
                db.Issues.Add(issue);
                await db.SaveChangesAsync();
                return issue;
            });
        }

        public Task Update(Issue issue)
        {
            return Query(db =>
            {
                db.Issues.Attach(issue);
                var entry = db.Entry(issue);
                entry.State = EntityState.Modified;
                db.Entry(issue).Property(x => x.DateCreate).IsModified = false;
                return db.SaveChangesAsync();
            });
        }

        public Task Delete(int id)
        {
            return Query(async db =>
            {
                if (await db.Issues.AnyAsync(it => it.ParentId == id))
                    throw new DataLayerException("Issue contains sub items");

                var issue = await db.Issues.SingleAsync(it=>it.Id == id);
                db.Issues.Remove(issue);
                return db.SaveChanges();
            });
        }
    }
}